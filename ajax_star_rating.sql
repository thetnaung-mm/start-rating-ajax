-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 29, 2017 at 06:33 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajax_star_rating`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `link`) VALUES
(1, 'Storing Data Locally in a PhoneGap App with SQLite', 'Data storing is a basic requirement while creating an application.\n\nIt is possible to store data online but the app needs to be online whenever data processing is required.\n\nFor local data storage use SQLite database which is already embedded on the mobile platforms - Android, IOS, Windows, Blackberry, etc.\n\nThe Cordova plugin provides support to access SQLite database in the app.\n\nIn this tutorial, I am creating an Android app where use SQLite database to save and retrieve records. Deploy the application with PhoneGap Build.', 'http://makitweb.com/storing-data-locally-in-a-phonegap-app-with-sqlite/'),
(2, 'jQuery UI autocomplete with PHP and AJAX', 'The autocomplete functionality shows the suggestion list according to the entered value. The user can select a value from the list.\n\nWith jQuery UI you can easily add autocomplete widget on the input element. Navigate to the suggestion list either by mouse or keyboard arrow keys.\n\nIt has the various options that allow us to customize the widget.', 'http://makitweb.com/jquery-ui-autocomplete-with-php-and-ajax/'),
(3, 'Add plugin to the Android app - PhoneGap', 'You cannot directly access the system feature in your PhoneGap app to extend its functionality.\n\nPhoneGap provide various plugins that allow accessing features like - camera, geolocation, contacts, battery status etc.', 'http://makitweb.com/add-plugin-to-the-android-app-phonegap/'),
(4, 'Insert record to Database Table - Codeigniter', 'All logical operation and database handling done in the Models like insert, update or fetch records.\n\nCodeigniter has predefined Database methods to perform database request.\n\nIn the demonstration, with View show the HTML form and when the user submits the forms then call the Model method to insert record from the controller.', 'http://makitweb.com/insert-record-to-database-table-codeigniter/'),
(5, 'How to install and setup Codeigniter', 'Codeigniter is a lightweight PHP-based framework to develop the web application. It is based on MVC (Model View Controller) pattern.\n', 'http://makitweb.com/how-to-install-and-setup-codeigniter/'),
(6, 'Add tooltip to the element with Bootstrap', 'Bootstrap provides dozens of custom plugins that helps to create better UI. With this, you can easily add tooltip to the HTML elements.\n\nA tooltip will appear when the user moves the mouse pointer over the element e.g. link, buttons, etc. This gives hint or quick information to the user.', 'http://makitweb.com/add-tooltip-to-the-element-with-bootstrap/'),
(7, 'Make android app with PhoneGap Build', 'PhoneGap is a framework that use to build mobile applications for multiple platforms - Android, iOS, Windows Phone, Blackberry etc.\n\nWith HTML, CSS, and JavaScript you can build an application.\n\nYou don\'t have to require to re-write code for other platforms.', 'http://makitweb.com/make-android-app-with-phonegap-build/'),
(8, 'How to handle AJAX request on the same page - PHP', 'AJAX is use to communicate with the server to perform the action without the need to refresh the page.\n\nYou can either handle AJAX requests on the same page or on the separate page.', 'http://makitweb.com/how-to-handle-ajax-request-on-the-same-page-php/'),
(9, 'Automatic page load progress bar with Pace.js', 'The pace.js is an automatic page load progress bar. You don\'t need to write any code to initialize the script during page load.\n\nIt is easy to implement and not dependent on any other external JavaScript libraries.', 'http://makitweb.com/automatic-page-load-progress-bar-with-pace-js/'),
(10, 'Remove unwanted whitespace from the column - MySQL', 'There is always the possibility that the users may not enter the values as we expected and the data is being saved on the Database table. E.g. unwanted whitespace or characters with the value.\n\nYou will see the issue when you check for duplicate records or sort the list.', 'http://makitweb.com/remove-unwanted-white-space-from-the-column-mysql/'),
(11, 'How to capture picture from webcam with Webcam.js', 'Webcam.js is a JavaScript library that allows us to capture picture from the webcam.\n\nIt uses HTML5 getUserMedia API to capture the picture. Flash is only used if the browser doesn’t support getUserMedia.', 'http://makitweb.com/how-to-capture-picture-from-webcam-with-webcam-js/'),
(12, 'Make Price Range Slider with AngularJS and PHP', 'Most of the eCommerce sites e.g. Flipkart, Snapdeal, etc have a price range slider for searching purpose.\n\nThe user doesn\'t have to enter price range manually.', 'http://makitweb.com/make-price-range-slider-with-angularjs-and-php/'),
(13, 'Create alphabetical pagination with PHP MySQL', 'The alphabetical pagination searches the records according to the first character in a specific column.\n\nYou can either manually fix the characters from A-Z or use the database table column value to create the list.', 'http://makitweb.com/create-alphabetical-pagination-with-php-mysql/'),
(14, 'Check if username exists with AngularJS', 'It is always a better idea to check the entered username exists or not if you are allowing the users to choose username while registration and wants it to be unique.\n\nWith this, the user will instantly know that their chosen username is available or not.', 'http://makitweb.com/check-if-username-exists-with-angularjs/'),
(15, 'How to avoid jQuery conflict with other JS libraries', 'By default, jQuery uses $ as an alias for jQuery because of this reason sometimes it conflicts with other JS libraries if they are also using $ as a function or variable name.', 'http://makitweb.com/how-to-avoid-jquery-conflict-with-other-js-libraries/'),
(16, 'Check if element has certain Class - jQuery', 'jQuery has inbuilt methods that allow searching for the certain class within the element.\n\nBy using them you can easily check class on the selector and perform the action according to the response.', 'http://makitweb.com/check-if-element-has-certain-class-jquery/'),
(17, 'How to show Weather widget on the Website', 'There are many sites which offer free weather widget for the website. That are easy to embed.\n\nYou only need to specify some of the mandatory fields for generating the code.', 'http://makitweb.com/how-to-show-weather-widget-on-the-website/'),
(18, 'Convert Unix timestamp to Date time with JavaScript', 'The Unix timestamp value conversion with JavaScript mainly requires when the API request response contains the date time value in Unix format and requires to show it on the screen in user readable form.', 'http://makitweb.com/convert-unix-timestamp-to-date-time-with-javascript/'),
(19, 'Make Carousel slider with Siema plugin - JavaScript', 'The Siema is a lightweight JavaScript plugin that adds carousel slider on the page. It is not dependent on any other plugins and not require to do any styling.\n\nIt is easy to setup on the page.', 'http://makitweb.com/make-carousel-slider-with-siema-plugin-javascript/'),
(20, 'Create autocomplete search with AngularJS and PHP', 'The autocomplete functionality gives the user suggestions based on its input. From there, it can select an option.\n\nIn the demonstration, I am creating a search box and display suggestion list whenever the user input value in the search box.', 'http://makitweb.com/create-autocomplete-search-with-angularjs-and-php/');

-- --------------------------------------------------------

--
-- Table structure for table `post_rating`
--

CREATE TABLE `post_rating` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `rating` int(2) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_rating`
--

INSERT INTO `post_rating` (`id`, `userid`, `postid`, `rating`, `timestamp`) VALUES
(1, 2, 1, 3, '2017-07-02 19:02:11'),
(2, 2, 3, 3, '2017-07-02 19:06:10'),
(3, 2, 4, 5, '2017-07-02 19:06:14'),
(4, 2, 5, 4, '2017-07-02 19:06:16'),
(5, 3, 2, 5, '2017-07-02 19:11:33'),
(6, 4, 2, 1, '2017-08-29 04:29:10'),
(7, 4, 1, 1, '2017-08-29 04:29:11'),
(8, 4, 10, 5, '2017-07-03 03:14:29'),
(9, 4, 11, 3, '2017-07-03 03:14:37'),
(10, 1, 1, 5, '2017-07-03 05:06:32'),
(11, 1, 4, 5, '2017-07-03 05:06:42'),
(12, 1, 3, 4, '2017-07-03 05:07:10'),
(13, 1, 10, 3, '2017-07-03 05:07:18'),
(14, 1, 9, 4, '2017-07-03 05:07:23'),
(15, 1, 11, 5, '2017-07-03 05:07:27'),
(16, 1, 19, 4, '2017-07-03 05:18:27'),
(17, 1, 18, 5, '2017-07-03 05:18:29'),
(18, 1, 20, 3, '2017-07-03 05:18:31'),
(19, 1, 17, 4, '2017-07-03 05:18:35'),
(20, 1, 16, 5, '2017-07-03 05:18:37'),
(21, 1, 15, 4, '2017-07-03 05:18:38'),
(22, 1, 14, 4, '2017-07-03 05:18:40'),
(23, 1, 13, 4, '2017-07-03 05:18:41'),
(24, 1, 12, 5, '2017-07-03 05:18:43'),
(25, 1, 8, 5, '2017-07-03 05:18:47'),
(26, 1, 7, 4, '2017-07-03 05:18:49'),
(27, 1, 6, 3, '2017-07-03 05:18:51'),
(28, 1, 5, 4, '2017-07-03 05:18:52'),
(29, 1, 2, 4, '2017-07-03 05:18:54'),
(30, 2, 2, 5, '2017-07-03 05:21:30'),
(31, 2, 6, 3, '2017-07-03 05:21:35'),
(32, 2, 7, 4, '2017-07-03 05:21:37'),
(33, 2, 8, 4, '2017-07-03 05:21:39'),
(34, 2, 9, 3, '2017-07-03 05:21:40'),
(35, 2, 10, 5, '2017-07-03 05:21:42'),
(36, 2, 11, 4, '2017-07-03 05:21:44'),
(37, 2, 12, 5, '2017-07-03 05:21:46'),
(38, 2, 13, 4, '2017-07-03 05:21:47'),
(39, 2, 14, 4, '2017-07-03 05:21:50'),
(40, 2, 15, 3, '2017-07-03 05:21:52'),
(41, 2, 16, 4, '2017-07-03 05:21:56'),
(42, 2, 17, 5, '2017-07-03 05:21:59'),
(43, 2, 18, 3, '2017-07-03 05:22:02'),
(44, 2, 19, 4, '2017-07-03 05:22:05'),
(45, 2, 20, 4, '2017-07-03 05:22:07'),
(46, 3, 1, 4, '2017-07-03 05:22:32'),
(47, 3, 3, 4, '2017-07-03 05:22:35'),
(48, 3, 4, 4, '2017-07-03 05:22:37'),
(49, 3, 5, 5, '2017-07-03 05:22:38'),
(50, 3, 6, 4, '2017-07-03 05:22:40'),
(51, 3, 7, 5, '2017-07-03 05:22:45'),
(52, 3, 8, 4, '2017-07-03 05:22:47'),
(53, 3, 9, 4, '2017-07-03 05:22:49'),
(54, 3, 10, 4, '2017-07-03 05:22:51'),
(55, 3, 11, 5, '2017-07-03 05:22:52'),
(56, 3, 12, 4, '2017-07-03 05:22:54'),
(57, 3, 13, 5, '2017-07-03 05:22:56'),
(58, 3, 14, 4, '2017-07-03 05:22:57'),
(59, 3, 15, 4, '2017-07-03 05:22:59'),
(60, 3, 16, 4, '2017-07-03 05:23:00'),
(61, 3, 17, 4, '2017-07-03 05:23:02'),
(62, 3, 18, 4, '2017-07-03 05:23:03'),
(63, 3, 19, 5, '2017-07-03 05:23:05'),
(64, 3, 20, 4, '2017-07-03 05:23:11'),
(65, 4, 3, 3, '2017-08-28 10:29:12'),
(66, 4, 4, 4, '2017-07-03 05:23:35'),
(67, 4, 20, 4, '2017-07-03 05:23:40'),
(68, 4, 19, 4, '2017-07-03 05:23:41'),
(69, 4, 18, 5, '2017-07-03 05:23:45'),
(70, 4, 17, 3, '2017-07-03 05:23:46'),
(71, 4, 16, 4, '2017-07-03 05:23:48'),
(72, 4, 15, 4, '2017-07-03 05:23:50'),
(73, 4, 13, 4, '2017-07-03 05:23:51'),
(74, 4, 14, 5, '2017-07-03 05:23:53'),
(75, 4, 12, 3, '2017-07-03 05:23:55'),
(76, 4, 8, 4, '2017-07-03 05:23:58'),
(77, 4, 9, 4, '2017-07-03 05:24:00'),
(78, 4, 7, 4, '2017-07-03 05:24:02'),
(79, 4, 6, 3, '2017-08-28 10:29:20'),
(80, 4, 5, 4, '2017-07-03 05:24:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_rating`
--
ALTER TABLE `post_rating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `post_rating`
--
ALTER TABLE `post_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
